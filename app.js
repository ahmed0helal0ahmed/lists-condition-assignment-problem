const app = Vue.createApp({
  data() {
    return {
      tasks: [],
      userInputTask: "",
      showTasks: true,
    };
  },
  methods: {
    addTask() {
      if (this.userInputTask) {
        this.tasks.push(this.userInputTask);
      }
      this.resetTaskInput();
    },
    resetTaskInput() {
      this.userInputTask = "";
    },
    toggleShowTasks() {
      this.showTasks = !this.showTasks;
    },
  },
  computed: {
    ToggleListOfTasks() {
      return this.showTasks ? "Hide" : "Show";
    },
  },
});

app.mount("#assignment");
